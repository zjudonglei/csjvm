package com.donglei.csjvm;

/**
 * @author donglei qq:285197243
 * @date 2021/8/29
 */
public class GCTest {
    public boolean b;
    public char c;
    public short s;
    public int i;
    public long j;
    public float f;
    public double d;
    public String obj = new String("aaa");
    public byte[] bytes = new byte[1024];

    public void test1() {
        long c = i + j;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            recursion(100);
            System.out.println("第" + i + "万次循环");
        }
    }

    static void recursion(int count) {
        GCTest test = new GCTest();
        test.test1();
        if (count > 0) {
            recursion(--count);
        }
    }
}
