package com.donglei.csjvm;

/**
 * @author donglei qq:285197243
 * @date 2021/8/22
 */
public class GaussTest {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}
