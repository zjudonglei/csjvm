package com.donglei.csjvm;

/**
 * @author donglei qq:285197243
 * @date 2021/8/26
 */
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, world!");
    }
}
