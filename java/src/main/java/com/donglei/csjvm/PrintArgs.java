package com.donglei.csjvm;

/**
 * @author donglei qq:285197243
 * @date 2021/8/26
 */
public class PrintArgs {
    public static void main(String[] args) {
        for (String arg : args) {
            System.out.println(arg);
        }
    }
}
