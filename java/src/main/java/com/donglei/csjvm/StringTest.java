package com.donglei.csjvm;

/**
 * @author donglei qq:285197243
 * @date 2021/8/28
 */
public class StringTest {
    public static void main(String[] args) {
        String s1 = "abc1";
        String s2 = "abc1";
        String s4 = new String("abc1");
        System.out.println(s1 == s2); // true
        System.out.println(s1 == s4); // false
        int x = 1;
        String s3 = "abc" + x;
        System.out.println(s1 == s3); // false
        s3 = s3.intern();
        System.out.println(s1 == s3); // true
    }
}
