﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jvm
{
    class Cmd
    {
        public bool helpFlag;
        public bool versionFlag;
        public string cpOption = "";
        public bool verboseClass;
        public bool verboseInst;
        public string xJreOption;
        public string clazz;
        public string runJar;
        public List<string> args = new List<string>();
        public int stackSize = 1024;

        public static Cmd Parse(string[] args)
        {
            Cmd cmd = new Cmd();
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-help":
                        cmd.helpFlag = true;
                        break;
                    case "-version":
                        cmd.versionFlag = true;
                        break;
                    case "-verbose":
                    case "-verbose:class":
                        cmd.verboseClass = true;
                        break;
                    case "-verbose:inst":
                        cmd.verboseInst = true;
                        break;
                    case "-cp":
                    case "-classpath":
                        cmd.cpOption = args[i + 1];
                        i++;
                        break;
                    case "-Xjre":
                        cmd.xJreOption = args[i + 1];
                        i++;
                        break;
                    case "-jar":
                        cmd.runJar = args[i + 1];
                        i++;
                        break;
                    default:
                        // 以第一个非格式化参数作为入口class
                        if ((cmd.runJar == null || cmd.runJar.Equals("")) &&
                            (cmd.clazz == null || cmd.clazz.Equals("")))
                        {
                            cmd.clazz = args[i];
                        }
                        else if (args[i].StartsWith("-"))
                        {

                        }
                        else
                        {
                            //Console.WriteLine("Usage: jvm [-options] class [args...]");
                            //Environment.Exit(0);

                            //cmd.args.Add(args[i++]);
                            cmd.args.Add(args[i]);
                        }
                        break;
                }
            }
            return cmd;
        }

        public void PrintUsage()
        {

        }
    }
}
