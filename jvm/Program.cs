﻿using jvm.classfile;
using jvm.classpath;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;

namespace jvm
{
    class Program
    {

        static void Main(string[] args)
        {
            Cmd cmd = Cmd.Parse(args);
            bool noClass = (cmd.runJar == null || cmd.runJar.Equals("")) &&
                            (cmd.clazz == null || cmd.clazz.Equals(""));
            if (cmd.versionFlag)
            {
                Console.Write("csjvm version 1.0.0");
            }
            else if (cmd.helpFlag || noClass)
            {
                cmd.PrintUsage();
            }
            else
            {
                new Jvm(cmd).Start();
            }
        }
    }
}
