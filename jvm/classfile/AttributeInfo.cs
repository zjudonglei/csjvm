﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
attribute_info {
    u2 attribute_name_index;
    u4 attribute_length;
    u1 info[attribute_length];
}
*/
namespace jvm.classfile
{
    abstract class AttributeInfo
    {
        public ConstantPool constantPool; // 缓存指引

        public ushort attribute_name_index; // 指向CpUtf8
        public uint attribute_length;

        public abstract void Read(ClassReader reader);
    }
}
