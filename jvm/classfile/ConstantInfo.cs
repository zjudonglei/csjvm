﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
cp_info {
    u1 tag;
    u1 info[];
}
*/
namespace jvm.classfile
{
    abstract class ConstantInfo
    {
        public byte tag;
        public ConstantPool pool;

        public abstract void Read(ClassReader reader);
    }
}
