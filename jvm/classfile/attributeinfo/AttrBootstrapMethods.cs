﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
BootstrapMethods_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 num_bootstrap_methods;
    {   u2 bootstrap_method_ref;
        u2 num_bootstrap_arguments;
        u2 bootstrap_arguments[num_bootstrap_arguments];
    } bootstrap_methods[num_bootstrap_methods];
}
*/
namespace jvm.classfile.attributeinfo
{
    class BootstrapMethod : AttributeInfo
    {
        public ushort bootstrapMethodRef;
        public ushort[] bootstrapArguments;

        public override void Read(ClassReader reader)
        {
            bootstrapMethodRef = reader.ReadUshort();
            ushort num_bootstrap_arguments = reader.ReadUshort();
            bootstrapArguments = new ushort[num_bootstrap_arguments];
            for (int i = 0; i < num_bootstrap_arguments; i++)
            {
                bootstrapArguments[i] = reader.ReadUshort();
            }
        }
    }

    class BootstrapMethodsAttribute : AttributeInfo
    {
        public BootstrapMethod[] bootstrapMethods;

        public override void Read(ClassReader reader)
        {
            ushort num_bootstrap_methods = reader.ReadUshort();
            bootstrapMethods = new BootstrapMethod[num_bootstrap_methods];
            for (int i = 0; i < num_bootstrap_methods; i++)
            {
                bootstrapMethods[i] = new BootstrapMethod();
                bootstrapMethods[i].Read(reader);
            }
        }
    }
}
