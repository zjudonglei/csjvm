﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
ConstantValue_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 constantvalue_index;
}
*/
namespace jvm.classfile.attributeinfo
{
    class ConstantValueAttribute : AttributeInfo
    {
        public ushort constantValueIndex;

        public override void Read(ClassReader reader)
        {
            constantValueIndex = reader.ReadUshort();
        }
    }
}
