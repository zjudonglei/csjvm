﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
EnclosingMethod_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 class_index;
    u2 method_index;
}
*/
namespace jvm.classfile.attributeinfo
{
    class EnclosingMethodAttribute : AttributeInfo
    {
        public ushort classIndex;
        public ushort methodIndex;

        public override void Read(ClassReader reader)
        {
            classIndex = reader.ReadUshort();
            methodIndex = reader.ReadUshort();
        }
    }
}
