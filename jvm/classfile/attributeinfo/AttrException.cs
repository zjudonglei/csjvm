﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Exceptions_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 number_of_exceptions;
    u2 exception_index_table[number_of_exceptions];
}
*/
namespace jvm.classfile.attributeinfo
{
    class ExceptionsAttribute : AttributeInfo
    {
        public ushort[] exceptionIndexTable;

        public override void Read(ClassReader reader)
        {
            ushort number_of_exceptions = reader.ReadUshort();
            exceptionIndexTable = new ushort[number_of_exceptions];
            for (int i = 0; i < number_of_exceptions; i++)
            {
                exceptionIndexTable[i] = reader.ReadUshort();
            }
        }
    }
}
