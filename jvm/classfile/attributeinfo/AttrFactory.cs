﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.classfile.attributeinfo
{
    class AttrFactory
    {
		/*
		attribute_info {
			u2 attribute_name_index;
			u4 attribute_length;
			u1 info[attribute_length];
		}
		*/
		public static AttributeInfo Create(ClassReader reader, ConstantPool constantPool)
        {
			ushort attrNameIndex = reader.ReadUshort();
			string attrName = constantPool.GetUtf8(attrNameIndex);
			uint attrLen = reader.ReadUint();
			AttributeInfo result;

			switch (attrName) {
				case "Code":
					result = new CodeAttribute();
					break;
				case "ConstantValue":
					result = new ConstantValueAttribute();
					break;
				case "Deprecated":
					result = new DeprecatedAttribute();
					break;
				case "Exceptions":
					result = new ExceptionsAttribute();
					break;
				case "LineNumberTable":
					result = new LineNumberTableAttribute();
					break;
				case "LocalVariableTable":
					result = new LocalVariableTableAttribute();
					break;
				case "SourceFile":
					result = new SourceFileAttribute();
					break;
				case "Synthetic":
					result = new SyntheticAttribute();
					break;
				default:
					result = new UnparsedAttribute();
					break;
			}

			result.constantPool = constantPool;
			result.attribute_name_index = attrNameIndex;
			result.attribute_length = attrLen;
			return result;
		}
    }
}
