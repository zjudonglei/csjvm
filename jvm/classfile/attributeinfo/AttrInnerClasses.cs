﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
InnerClasses_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 number_of_classes;
    {   u2 inner_class_info_index;
        u2 outer_class_info_index;
        u2 inner_name_index;
        u2 inner_class_access_flags;
    } classes[number_of_classes];
}
*/
namespace jvm.classfile.attributeinfo
{
    class InnerClassInfo : AttributeInfo
    {
        public ushort innerClassInfoIndex;
        public ushort outerClassInfoIndex;
        public ushort innerNameIndex;
        public ushort innerClassAccessFlags;

        public override void Read(ClassReader reader)
        {
            innerClassInfoIndex = reader.ReadUshort();
            outerClassInfoIndex = reader.ReadUshort();
            innerNameIndex = reader.ReadUshort();
            innerClassAccessFlags = reader.ReadUshort();
        }
    }

    class InnerClassesAttribute : AttributeInfo
    {
        public InnerClassInfo[] classes;

        public override void Read(ClassReader reader)
        {
            ushort number_of_classes = reader.ReadUshort();
            classes = new InnerClassInfo[number_of_classes];
            for (int i = 0; i < number_of_classes; i++)
            {
                classes[i] = new InnerClassInfo();
                classes[i].Read(reader);
            }
        }
    }
}
