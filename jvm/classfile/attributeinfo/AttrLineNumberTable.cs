﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
LineNumberTable_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 line_number_table_length;
    {   u2 start_pc;
        u2 line_number;
    } line_number_table[line_number_table_length];
}
*/
namespace jvm.classfile.attributeinfo
{
    class LineNumberTableEntry : AttributeInfo
    {
        public ushort startPc;
        public ushort lineNumber;

        public override void Read(ClassReader reader)
        {
            startPc = reader.ReadUshort();
            lineNumber = reader.ReadUshort();
        }
    }

    class LineNumberTableAttribute : AttributeInfo
    {
        public LineNumberTableEntry[] lineNumberTable;

        public override void Read(ClassReader reader)
        {
            ushort line_number_table_length = reader.ReadUshort();
            lineNumberTable = new LineNumberTableEntry[line_number_table_length];
            for (int i = 0; i < line_number_table_length; i++)
            {
                lineNumberTable[i] = new LineNumberTableEntry();
                lineNumberTable[i].Read(reader);
            }
        }

        public int GetLineNumber(int pc)
        {
            foreach (LineNumberTableEntry entry in lineNumberTable)
            {
                if (pc > entry.startPc)
                {
                    return entry.lineNumber;
                }
            }
            return -1;
        }
    }
}
