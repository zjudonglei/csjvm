﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
LocalVariableTable_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 local_variable_table_length;
    {   u2 start_pc;
        u2 length;
        u2 name_index;
        u2 descriptor_index;
        u2 index;
    } local_variable_table[local_variable_table_length];
}
*/
namespace jvm.classfile.attributeinfo
{
    class LocalVariableTableEntry : AttributeInfo
    {
        public ushort startPc;
        public ushort length;
        public ushort nameIndex;
        public ushort descriptor;
        public ushort index;

        public override void Read(ClassReader reader)
        {
            startPc = reader.ReadUshort();
            length = reader.ReadUshort();
            nameIndex = reader.ReadUshort();
            descriptor = reader.ReadUshort();
            index = reader.ReadUshort();
        }
    }

    class LocalVariableTableAttribute : AttributeInfo
    {
        public LocalVariableTableEntry[] localVariableTable;

        public override void Read(ClassReader reader)
        {
            ushort local_variable_table_length = reader.ReadUshort();
            localVariableTable = new LocalVariableTableEntry[local_variable_table_length];
            for (int i = 0; i < local_variable_table_length; i++)
            {
                localVariableTable[i] = new LocalVariableTableEntry();
                localVariableTable[i].Read(reader);
            }
        }
    }
}
