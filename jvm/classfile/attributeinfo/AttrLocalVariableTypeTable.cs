﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
LocalVariableTypeTable_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 local_variable_type_table_length;
    {
        u2 start_pc;
        u2 length;
        u2 name_index;
        u2 signature_index;
        u2 index;
    } local_variable_type_table[local_variable_type_table_length];
}
*/
namespace jvm.classfile.attributeinfo
{
    class LocalVariableTypeTableEntry : AttributeInfo
    {
        public ushort startPc;
        public ushort length;
        public ushort nameIndex;
        public ushort signatureIndex;
        public ushort index;

        public override void Read(ClassReader reader)
        {
            startPc = reader.ReadUshort();
            length = reader.ReadUshort();
            nameIndex = reader.ReadUshort();
            signatureIndex = reader.ReadUshort();
            index = reader.ReadUshort();
        }
    }

    class AttrLocalVariableTypeTable : AttributeInfo
    {
        public LocalVariableTypeTableEntry[] localVariableTypeTableEntrie;

        public override void Read(ClassReader reader)
        {
            ushort local_variable_type_table_length = reader.ReadUshort();
            localVariableTypeTableEntrie = new LocalVariableTypeTableEntry[local_variable_type_table_length];
            for (int i = 0; i < local_variable_type_table_length; i++)
            {
                localVariableTypeTableEntrie[i] = new LocalVariableTypeTableEntry();
                localVariableTypeTableEntrie[i].Read(reader);
            }
        }
    }
}
