﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.classfile.attributeinfo
{
    class MarkerAttribute : AttributeInfo
    {
        public override void Read(ClassReader reader)
        {
        }
    }

    /*
    Deprecated_attribute {
        u2 attribute_name_index;
        u4 attribute_length;
    }
    */
    class DeprecatedAttribute : MarkerAttribute
    {

    }

    /*
    Synthetic_attribute {
        u2 attribute_name_index;
        u4 attribute_length;
    }
    */
    class SyntheticAttribute : MarkerAttribute
    {

    }
}
