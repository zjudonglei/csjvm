﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Signature_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 signature_index;
}
*/
namespace jvm.classfile.attributeinfo
{
    class SignatureAttribute : AttributeInfo
    {
        public ushort signatureIndex;

        public override void Read(ClassReader reader)
        {
            signatureIndex = reader.ReadUshort();
        }
    }
}
