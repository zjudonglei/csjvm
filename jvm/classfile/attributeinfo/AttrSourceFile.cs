﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
SourceFile_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 sourcefile_index;
}
*/
namespace jvm.classfile.attributeinfo
{
    class SourceFileAttribute : AttributeInfo
    {
        public ushort sourceFileIndex;

        public override void Read(ClassReader reader)
        {
            sourceFileIndex = reader.ReadUshort();
        }

        public string FileName
        {
            get { return constantPool.GetUtf8(sourceFileIndex); }
        }
    }
}
