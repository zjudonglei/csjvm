﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
attribute_info {
    u2 attribute_name_index;
    u4 attribute_length;
    u1 info[attribute_length];
}
*/
namespace jvm.classfile.attributeinfo
{
    class UnparsedAttribute : AttributeInfo
    {
        public byte[] info;

        public override void Read(ClassReader reader)
        {
            info = reader.ReadBytes(attribute_length);
        }
    }
}
