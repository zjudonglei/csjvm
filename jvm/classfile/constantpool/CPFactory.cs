﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.classfile.constantpool
{
    class CPFactory
    {

        /*
        cp_info {
            u1 tag;
            u1 info[];
        }
        */
        public static ConstantInfo Create(ClassReader reader)
        {
            byte tag = reader.ReadByte();
            ConstantInfo result = null; ;
            switch ((ConstantEnum)tag)
            {
                case ConstantEnum.CONSTANT_Integer:
                    result = new ConstantIntegerInfo();
                    break;
                case ConstantEnum.CONSTANT_Long:
                    result = new ConstantLongInfo();
                    break;
                case ConstantEnum.CONSTANT_Float:
                    result = new ConstantFloatInfo();
                    break;
                case ConstantEnum.CONSTANT_Double:
                    result = new ConstantDoubleInfo();
                    break;
                case ConstantEnum.CONSTANT_Utf8:
                    result = new ConstantUtf8Info();
                    break;
                case ConstantEnum.CONSTANT_String:
                    result = new ConstantStringInfo();
                    break;
                case ConstantEnum.CONSTANT_Class:
                    result = new ConstantClassInfo();
                    break;
                case ConstantEnum.CONSTANT_Fieldref:
                    result = new ConstantFieldrefInfo();
                    break;
                case ConstantEnum.CONSTANT_Methodref:
                    result = new ConstantMethodrefInfo();
                    break;
                case ConstantEnum.CONSTANT_InterfaceMethodref:
                    result = new ConstantInterfaceMethodrefInfo();
                    break;
                case ConstantEnum.CONSTANT_NameAndType:
                    result = new ConstantNameAndTypeInfo();
                    break;
                case ConstantEnum.CONSTANT_MethodHandle:
                    result = new ConstantMethodHandleInfo();
                    break;
                case ConstantEnum.CONSTANT_MethodType:
                    result = new ConstantMethodTypeInfo();
                    break;
                case ConstantEnum.CONSTANT_InvokeDynamic:
                    result = new ConstantInvokeDynamicInfo();
                    break;
                default:
                    Console.WriteLine("java.lang.ClassFormatError: constant pool tag {0}!", tag);
                    Environment.Exit(0);
                    break;
            }
            result.tag = tag;
            return result;
        }

    }
}
