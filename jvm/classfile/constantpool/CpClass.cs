﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
CONSTANT_Class_info {
    u1 tag;
    u2 name_index;
}
*/
namespace jvm.classfile.constantpool
{
    class ConstantClassInfo : ConstantInfo
    {
        public ushort nameIndex;

        public override void Read(ClassReader reader)
        {
            nameIndex = reader.ReadUshort();
        }

        public string Name()
        {
            return pool.GetUtf8(nameIndex);
        }
    }
}
