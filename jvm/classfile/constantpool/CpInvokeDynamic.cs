﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.classfile.constantpool
{
    /*
   CONSTANT_MethodHandle_info {
       u1 tag;
       u1 reference_kind;
       u2 reference_index;
   }
   */
    class ConstantMethodHandleInfo : ConstantInfo
    {
        public byte referenceKind;
        public ushort referenceIndex;

        public override void Read(ClassReader reader)
        {
            referenceKind = reader.ReadByte();
            referenceIndex = reader.ReadUshort();
        }
    }

    /*
    CONSTANT_MethodType_info {
        u1 tag;
        u2 descriptor_index;
    }
    */
    class ConstantMethodTypeInfo : ConstantInfo
    {
        public ushort descriptorIndex;

        public override void Read(ClassReader reader)
        {
            descriptorIndex = reader.ReadUshort();
        }
    }

    /*
    CONSTANT_InvokeDynamic_info {
        u1 tag;
        u2 bootstrap_method_attr_index;
        u2 name_and_type_index;
    }
    */
    class ConstantInvokeDynamicInfo : ConstantInfo
    {
        public ushort bootstrapMethodAttrIndex;
        public ushort nameAndTypeIndex;

        public override void Read(ClassReader reader)
        {
            bootstrapMethodAttrIndex = reader.ReadUshort();
            nameAndTypeIndex = reader.ReadUshort();
        }
    }
}
