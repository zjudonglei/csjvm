﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
CONSTANT_Fieldref_info {
    u1 tag;
    u2 class_index;
    u2 name_and_type_index;
}
CONSTANT_Methodref_info {
    u1 tag;
    u2 class_index;
    u2 name_and_type_index;
}
CONSTANT_InterfaceMethodref_info {
    u1 tag;
    u2 class_index;
    u2 name_and_type_index;
}
*/
namespace jvm.classfile.constantpool
{
    class ConstantMemberrefInfo : ConstantInfo
    {
        public ushort classIndex; // 指向CpClass
        public ushort nameAndTypeIndex; // 指向CpNameAndType

        public override void Read(ClassReader reader)
        {
            classIndex = reader.ReadUshort();
            nameAndTypeIndex = reader.ReadUshort();
        }

        public string ClassName()
        {
            return pool.GetClassName(classIndex);
        }
    }

    class ConstantFieldrefInfo : ConstantMemberrefInfo
    {

    }

    class ConstantMethodrefInfo : ConstantMemberrefInfo
    {

    }

    class ConstantInterfaceMethodrefInfo : ConstantMemberrefInfo
    {

    }
}
