﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
CONSTANT_NameAndType_info {
    u1 tag;
    u2 name_index;
    u2 descriptor_index;
}
*/
namespace jvm.classfile.constantpool
{
    class ConstantNameAndTypeInfo : ConstantInfo
    {
        public ushort nameIndex; // 全都是指向CpUtf8的索引
        public ushort descriptorIndex;

        public override void Read(ClassReader reader)
        {
            nameIndex = reader.ReadUshort();
            descriptorIndex = reader.ReadUshort();
        }
    }
}
