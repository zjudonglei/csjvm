﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.classfile.constantpool
{
    /*
    CONSTANT_Integer_info {
        u1 tag;
        u4 bytes;
    }
    */
    class ConstantIntegerInfo : ConstantInfo
    {
        public int val;

        public override void Read(ClassReader reader)
        {
            val = reader.ReadInt();
        }
    }

    /*
    CONSTANT_Float_info {
        u1 tag;
        u4 bytes;
    }
    */
    class ConstantFloatInfo : ConstantInfo
    {
        public float val;
        public override void Read(ClassReader reader)
        {
            val = reader.ReadFloat();
        }
    }

    /*
    CONSTANT_Long_info {
        u1 tag;
        u4 high_bytes;
        u4 low_bytes;
    }
    */
    class ConstantLongInfo : ConstantInfo
    {
        public long val;
        public override void Read(ClassReader reader)
        {
            val = reader.ReadLong();
        }
    }

    /*
    CONSTANT_Double_info {
        u1 tag;
        u4 high_bytes;
        u4 low_bytes;
    }
    */
    class ConstantDoubleInfo : ConstantInfo
    {
        public double val;
        public override void Read(ClassReader reader)
        {
            val = reader.ReadDouble();
        }
    }
}
