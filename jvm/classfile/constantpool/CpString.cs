﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
CONSTANT_String_info {
    u1 tag;
    u2 string_index;
}
*/
namespace jvm.classfile.constantpool
{
    class ConstantStringInfo : ConstantInfo
    {
        public ushort stringIndex; // 指向CpUtf8的索引

        public override void Read(ClassReader reader)
        {
            stringIndex = reader.ReadUshort();
        }

        public string String()
        {
            return pool.GetUtf8(stringIndex);
        }
    }
}
