﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
CONSTANT_Utf8_info {
    u1 tag;
    u2 length;
    u1 bytes[length];
}
*/
namespace jvm.classfile.constantpool
{
    class ConstantUtf8Info : ConstantInfo
    {
        public string str;

        public override void Read(ClassReader reader)
        {
            str = reader.ReadString();
        }
    }
}
