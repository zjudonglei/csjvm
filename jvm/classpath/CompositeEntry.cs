﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jvm.classpath
{
    class CompositeEntry : Entry
    {
        List<Entry> entries = new List<Entry>();
        public CompositeEntry(string path) : base(path)
        {
            string[] paths = path.Split(EntryFactory.PathListSeparator);
            foreach (string eachPath in paths)
            {
                entries.Add(EntryFactory.Create(eachPath));
            }
        }

        public override Tuple<bool, byte[], Entry> ReadClass(string classname)
        {
            foreach (Entry entry in entries)
            {
                Tuple<bool, byte[], Entry> result = entry.ReadClass(classname);
                if (result.Item1)
                {
                    return result;
                }
            }
            return new Tuple<bool, byte[], Entry>(false, null, null);
        }

    }
}
