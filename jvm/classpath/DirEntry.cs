﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jvm.classpath
{
    class DirEntry : Entry
    {
        public DirEntry(string path) : base(path)
        {
        }

        public override Tuple<bool, byte[], Entry> ReadClass(string classname)
        {
            string filePath = Path.Combine(path, classname);
            if (File.Exists(filePath))
            {
                return new Tuple<bool, byte[], Entry>(true, File.ReadAllBytes(filePath), this);
            }
            return new Tuple<bool, byte[], Entry>(false, null, this);
        }

    }
}
