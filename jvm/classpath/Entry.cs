﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jvm.classpath
{
    abstract class Entry
    {
        protected string path;
        public Entry(string path)
        {
            this.path = path;
		}

        public abstract Tuple<bool, byte[], Entry> ReadClass(string classname);

        public override string ToString()
        {
            return path;
        }
    }


}
