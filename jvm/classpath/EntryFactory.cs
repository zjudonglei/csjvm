﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jvm.classpath
{
    class EntryFactory
    {
		public const char PathListSeparator = ';';
		public static Entry Create(string path)
        {
			if (path.Contains(PathListSeparator))
			{
				return new CompositeEntry(path);
			}

			if (path.EndsWith("*"))
			{
				return new WildcardEntry(path);
			}

			if (path.EndsWith(".jar") || path.EndsWith(".JAR") ||
				path.EndsWith(".zip") || path.EndsWith(".ZIP"))
			{
				return new JarEntry(path);
			}

			return new DirEntry(path);
		}
	}
}
