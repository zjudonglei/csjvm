﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jvm.classpath
{
    class WildcardEntry : Entry
    {
        List<Entry> entries = new List<Entry>();
        public WildcardEntry(string path) : base(path)
        {
            path = path.Substring(0, path.LastIndexOf('*'));
            DirectoryInfo folder = new DirectoryInfo(path);
            foreach (FileInfo file in folder.GetFiles())
            {
                if (file.FullName.EndsWith(".jar") || file.FullName.EndsWith(".JAR"))
                {
                    entries.Add(EntryFactory.Create(file.FullName));
                }
            }
        }

        public override Tuple<bool, byte[], Entry> ReadClass(string classname)
        {
            foreach (Entry entry in entries)
            {
                Tuple<bool, byte[], Entry> result = entry.ReadClass(classname);
                if (result.Item1)
                {
                    return result;
                }
            }
            return new Tuple<bool, byte[], Entry>(false, null, this);
        }
    }
}
