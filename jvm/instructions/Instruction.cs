﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions
{
    abstract class Instruction
    {
        public abstract void FetchOperands(ByteCodeReader reader);
        public abstract void Execute(JFrame frame);
    }
}
