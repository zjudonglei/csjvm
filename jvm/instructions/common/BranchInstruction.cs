﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.common
{
    abstract class BranchInstruction : Instruction
    {
        public int offset;

        public override void FetchOperands(ByteCodeReader reader)
        {
            offset = reader.ReadInt16();
        }
    }
}
