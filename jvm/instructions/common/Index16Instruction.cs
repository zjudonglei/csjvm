﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.common
{
    abstract class Index16Instruction : Instruction
    {
        public ushort index;

        public override void FetchOperands(ByteCodeReader reader)
        {
            index = reader.ReadUint16();
        }
    }
}
