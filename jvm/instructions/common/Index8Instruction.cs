﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.common
{
    abstract class Index8Instruction : Instruction
    {
        public uint index;

        public override void FetchOperands(ByteCodeReader reader)
        {
            index = reader.ReadUint8();
        }
    }
}
