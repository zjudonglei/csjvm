﻿using jvm.rtda;
using jvm.rtda.frame;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class InvokeUtil
    {
        public static void InvokeMethod(JFrame invokerFrame, JMethod method)
        {
            JThread thread = invokerFrame.thread;
            JFrame newFrame = thread.NewFrame(method);
            thread.PushFrame(newFrame);

            // 从调用者栈将参数压入新帧的栈
            int argCount = (int)method.argCount;
            if (argCount > 0)
            {
                for (int i = argCount - 1; i >= 0; i--)
                {
                    Slot slot = invokerFrame.operandStack.PopSlot();
                    newFrame.localVars.SetSlot((uint)i, slot);
                }
            }

            //if (method.Is(AccessFlags.ACC_NATIVE))
            //{
            //    if (method.name.Equals("registerNatives"))
            //    {
            //        thread.PopFrame();
            //    }
            //    else
            //    {
            //        Console.WriteLine("native method: {0}.{0} {1}", method.jClass.name, method.name, method.descriptor);
            //        Environment.Exit(0);
            //    }
            //}
        }
    }
}
