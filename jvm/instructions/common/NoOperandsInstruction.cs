﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.common
{
    abstract class NoOperandsInstruction : Instruction
    {
        public override void FetchOperands(ByteCodeReader reader)
        {
            
        }
    }
}
