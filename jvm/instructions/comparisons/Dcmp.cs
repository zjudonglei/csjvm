﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class DcmpUtil
    {
        public static void dcmp(JFrame frame, bool gFlag)
        {
            JOperandStack stack = frame.operandStack;
            double v2 = stack.PopDouble();
            double v1 = stack.PopDouble();
            if (v1 > v2)
            {
                stack.PushInt(1);
            }
            else if (v1 == v2)
            {
                stack.PushInt(0);
            }
            else if (v1 < v2)
            {
                stack.PushInt(-1);
            }
            else if (gFlag)
            {
                stack.PushInt(1);
            }
            else
            {
                stack.PushInt(-1);
            }
        }
    }

    class DCMPG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DcmpUtil.dcmp(frame, true);
        }
    }

    class DCMPL : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DcmpUtil.dcmp(frame, false);
        }
    }
}
