﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class FcmpUtil
    {
        public static void fcmp(JFrame frame, bool gFlag)
        {
            JOperandStack stack = frame.operandStack;
            double v2 = stack.PopFloat();
            double v1 = stack.PopFloat();
            if (v1 > v2)
            {
                stack.PushInt(1);
            }
            else if (v1 == v2)
            {
                stack.PushInt(0);
            }
            else if (v1 < v2)
            {
                stack.PushInt(-1);
            }
            else if (gFlag)
            {
                stack.PushInt(1);
            }
            else
            {
                stack.PushInt(-1);
            }
        }
    }

    class FCMPG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FcmpUtil.fcmp(frame, true);
        }
    }

    class FCMPL : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FcmpUtil.fcmp(frame, false);
        }
    }
}
