﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class IfAcmpUtil
    {
        public static bool _acmp(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            JObject ref2 = stack.PopRef();
            JObject ref1 = stack.PopRef();
            return ref1 == ref2;
        }
    }

    class IF_ACMPEQ : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            if (IfAcmpUtil._acmp(frame))
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ACMPNE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            if (!IfAcmpUtil._acmp(frame))
            {
                frame.Branch(offset);
            }
        }
    }
}
