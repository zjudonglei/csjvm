﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class IFEQ : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val == 0)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFNE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val != 0)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFLT : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val < 0)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFLE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val <= 0)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFGT : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val > 0)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFGE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            if (val >= 0)
            {
                frame.Branch(offset);
            }
        }
    }
}
