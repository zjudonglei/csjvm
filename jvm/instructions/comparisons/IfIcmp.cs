﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class IfIcmpUtil
    {
        public static Tuple<int, int> _icmpPop(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val2 = stack.PopInt();
            int val1 = stack.PopInt();
            return new Tuple<int, int>(val1, val2);
        }
    }

    class IF_ICMPEQ : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 == tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ICMPNE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 != tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ICMPLT : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 < tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ICMPLE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 <= tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ICMPGT : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 > tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }

    class IF_ICMPGE : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            Tuple<int, int> tuple = IfIcmpUtil._icmpPop(frame);
            if (tuple.Item1 >= tuple.Item2)
            {
                frame.Branch(offset);
            }
        }
    }
}
