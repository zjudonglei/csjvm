﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.comparisons
{
    class LCMP : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long v2 = stack.PopLong();
            long v1 = stack.PopLong();
            if (v1 > v2)
            {
                stack.PushInt(1);
            }
            else if (v1 == v2)
            {
                stack.PushInt(0);
            }
            else
            {
                stack.PushInt(-1);
            }
        }
    }

}
