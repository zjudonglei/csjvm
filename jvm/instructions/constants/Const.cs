﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.constants
{
    class ACONST_NULL : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushRef(null);
        }
    }

    class DCONST_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushDouble(0D);
        }
    }

    class DCONST_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushDouble(1D);
        }
    }

    class FCONST_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushFloat(0F);
        }
    }

    class FCONST_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushFloat(1F);
        }
    }

    class FCONST_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushFloat(2F);
        }
    }

    class ICONST_M1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(-1);
        }
    }

    class ICONST_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(0);
        }
    }

    class ICONST_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(1);
        }
    }

    class ICONST_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(2);
        }
    }

    class ICONST_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(3);
        }
    }

    class ICONST_4 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(4);
        }
    }

    class ICONST_5 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(5);
        }
    }

    class LCONST_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushLong(0L);
        }
    }

    class LCONST_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushLong(1L);
        }
    }
}
