﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.constants
{
    class NOP : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {

        }
    }
}
