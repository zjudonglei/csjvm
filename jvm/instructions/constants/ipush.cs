﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.constants
{
    class BIPUSH : Instruction
    {
        sbyte val;

        public override void FetchOperands(ByteCodeReader reader)
        {
            val = reader.ReadInt8();
        }

        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(val);
        }
    }

    class SIPUSH : Instruction
    {
        short val;

        public override void FetchOperands(ByteCodeReader reader)
        {
            val = reader.ReadInt16();
        }

        public override void Execute(JFrame frame)
        {
            frame.operandStack.PushInt(val);
        }
    }
}
