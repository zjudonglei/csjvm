﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.control
{
    class GOTO : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.Branch(offset);
        }
    }
}
