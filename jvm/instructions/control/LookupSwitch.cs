﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.control
{
    class LOOKUP_SWITCH : Instruction
    {
        public int defaultOffset;
        public int count;
        public int[] offsets;

        public override void FetchOperands(ByteCodeReader reader)
        {
            reader.SkipPadding();
            defaultOffset = reader.ReadInt32();
            count = reader.ReadInt32();
            offsets = reader.ReadInt32s(count * 2);
        }

        public override void Execute(JFrame frame)
        {
            int key = frame.operandStack.PopInt();
            for (int i = 0; i < count * 2; i += 2)
            {
                if (offsets[i] == key)
                {
                    frame.Branch(offsets[i + 1]);
                    return;
                }
            }
            frame.Branch(defaultOffset);
        }

    }
}
