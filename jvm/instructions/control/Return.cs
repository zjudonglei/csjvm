﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.control
{
    class RETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            frame.thread.PopFrame();
        }
    }

    class ARETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JThread thread = frame.thread;
            JFrame curFrame = thread.PopFrame();
            JFrame invokerFrame = thread.TopFrame();
            JObject obj = curFrame.operandStack.PopRef();
            invokerFrame.operandStack.PushRef(obj);
        }
    }

    class DRETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JThread thread = frame.thread;
            JFrame curFrame = thread.PopFrame();
            JFrame invokerFrame = thread.TopFrame();
            double val = curFrame.operandStack.PopDouble();
            invokerFrame.operandStack.PushDouble(val);
        }
    }

    class FRETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JThread thread = frame.thread;
            JFrame curFrame = thread.PopFrame();
            JFrame invokerFrame = thread.TopFrame();
            float val = curFrame.operandStack.PopFloat();
            invokerFrame.operandStack.PushFloat(val);
        }
    }

    class IRETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JThread thread = frame.thread;
            JFrame curFrame = thread.PopFrame();
            JFrame invokerFrame = thread.TopFrame();
            int val = curFrame.operandStack.PopInt();
            invokerFrame.operandStack.PushInt(val);
        }
    }

    class LRETURN : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JThread thread = frame.thread;
            JFrame curFrame = thread.PopFrame();
            JFrame invokerFrame = thread.TopFrame();
            long val = curFrame.operandStack.PopLong();
            invokerFrame.operandStack.PushLong(val);
        }
    }
}
