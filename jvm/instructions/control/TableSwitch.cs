﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.control
{
    class TABLE_SWITCH : Instruction
    {
        public int defaultOffset;
        public int low;
        public int high;
        public int[] offsets;

        public override void FetchOperands(ByteCodeReader reader)
        {
            reader.SkipPadding();
            defaultOffset = reader.ReadInt32();
            low = reader.ReadInt32();
            high = reader.ReadInt32();
            int count = high - low + 1;
            offsets = reader.ReadInt32s(count);
        }

        public override void Execute(JFrame frame)
        {
            int index = frame.operandStack.PopInt();
            if (index >= low && index <= high)
            {
                frame.Branch(index - low);
            }
            else
            {
                frame.Branch(defaultOffset);
            }
        }
    }
}
