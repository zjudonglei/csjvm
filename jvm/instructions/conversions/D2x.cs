﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.conversions
{
    class D2F : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            double d = stack.PopDouble();
            float f = (float)d;
            stack.PushFloat(f);
        }
    }

    class D2I : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            double d = stack.PopDouble();
            int i = (int)d;
            stack.PushInt(i);
        }
    }

    class D2L : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            double d = stack.PopDouble();
            long i = (long)d;
            stack.PushLong(i);
        }
    }
}
