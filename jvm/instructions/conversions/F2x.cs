﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.conversions
{
    class F2D : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            float f = stack.PopFloat();
            double d = f;
            stack.PushDouble(d);
        }
    }

    class F2I : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            float f = stack.PopFloat();
            int i = (int)f;
            stack.PushInt(i);
        }
    }

    class F2L : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            float f = stack.PopFloat();
            long l = (long)f;
            stack.PushLong(l);
        }
    }
}
