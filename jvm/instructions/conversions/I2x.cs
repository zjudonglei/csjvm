﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.conversions
{
    class I2B : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            byte b = (byte)i;
            stack.PushInt(b);
        }
    }

    class I2C : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            char c = (char)i;
            stack.PushInt(c);
        }
    }

    class I2S : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            short s = (short)i;
            stack.PushInt(s);
        }
    }

    class I2L : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            long l = i;
            stack.PushLong(l);
        }
    }

    class I2F : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            float f = i;
            stack.PushFloat(f);
        }
    }

    class I2D : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int i = stack.PopInt();
            double d = i;
            stack.PushDouble(d);
        }
    }
}
