﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.conversions
{
    class L2D : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long l = stack.PopLong();
            double d = l;
            stack.PushDouble(d);
        }
    }

    class L2F : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long l = stack.PopLong();
            float f = l;
            stack.PushFloat(f);
        }
    }

    class L2I : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long l = stack.PopLong();
            int i = (int)l;
            stack.PushInt(i);
        }
    }
}
