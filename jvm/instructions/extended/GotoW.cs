﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.extended
{
    class GOTO_W : Instruction
    {
        int offset;
        public override void FetchOperands(ByteCodeReader reader)
        {
            offset = reader.ReadInt32();
        }
        public override void Execute(JFrame frame)
        {
            frame.Branch(offset);
        }
    }
}
