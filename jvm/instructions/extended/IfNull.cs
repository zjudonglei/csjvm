﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.extended
{
    class IFNULL : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JObject obj = frame.operandStack.PopRef();
            if (obj == null)
            {
                frame.Branch(offset);
            }
        }
    }

    class IFNONNULL : BranchInstruction
    {
        public override void Execute(JFrame frame)
        {
            JObject obj = frame.operandStack.PopRef();
            if (obj != null)
            {
                frame.Branch(offset);
            }
        }
    }
}
