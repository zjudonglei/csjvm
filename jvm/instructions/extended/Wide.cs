﻿using jvm.instructions.common;
using jvm.instructions.loads;
using jvm.instructions.math;
using jvm.instructions.stores;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.extended
{
    class WIDE : Instruction
    {
        Instruction modifiedInstruction;
        public override void FetchOperands(ByteCodeReader reader)
        {
            byte opcode = reader.ReadUint8();
            switch (opcode)
            {
                case 0x15:
                    ILOAD iLOAD = new ILOAD();
                    iLOAD.index = (reader.ReadUint16());
                    modifiedInstruction = iLOAD;
                    break;
                case 0x16:
                    LLOAD lLOAD = new LLOAD();
                    lLOAD.index = (reader.ReadUint16());
                    modifiedInstruction = lLOAD;
                    break;
                case 0x17:
                    FLOAD fLOAD = new FLOAD();
                    fLOAD.index = (reader.ReadUint16());
                    modifiedInstruction = fLOAD;
                    break;
                case 0x18:
                    DLOAD dLOAD = new DLOAD();
                    dLOAD.index = (reader.ReadUint16());
                    modifiedInstruction = dLOAD;
                    break;
                case 0x19:
                    ALOAD aLOAD = new ALOAD();
                    aLOAD.index = (reader.ReadUint16());
                    modifiedInstruction = aLOAD;
                    break;

                case 0x36:
                    ISTORE iSTORE = new ISTORE();
                    iSTORE.index = (reader.ReadUint16());
                    modifiedInstruction = iSTORE;
                    break;
                case 0x37:
                    LSTORE lSTORE = new LSTORE();
                    lSTORE.index = (reader.ReadUint16());
                    modifiedInstruction = lSTORE;
                    break;
                case 0x38:
                    FSTORE fSTORE = new FSTORE();
                    fSTORE.index = (reader.ReadUint16());
                    modifiedInstruction = fSTORE;
                    break;
                case 0x39:
                    DSTORE dSTORE = new DSTORE();
                    dSTORE.index = (reader.ReadUint16());
                    modifiedInstruction = dSTORE;
                    break;
                case 0x3a:
                    ASTORE aSTORE = new ASTORE();
                    aSTORE.index = (reader.ReadUint16());
                    modifiedInstruction = aSTORE;
                    break;

                case 0x84:
                    IINC iINC = new IINC();
                    iINC.index = (reader.ReadUint16());
                    iINC.val = (reader.ReadInt16());
                    modifiedInstruction = iINC;
                    break;
                case 0xa9:
                    Console.WriteLine("Unsupported opcode: 0xa9!");
                    break;
            }
        }
        public override void Execute(JFrame frame)
        {
            modifiedInstruction.Execute(frame);
        }

    }
}
