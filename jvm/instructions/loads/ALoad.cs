﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.loads
{
    class ALoadUtil
    {
        public static void _aload(JFrame frame, uint index)
        {
            JObject obj = frame.localVars.GetRef(index);
            frame.operandStack.PushRef(obj);
        }
    }

    class ALOAD : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            ALoadUtil._aload(frame, index);
        }
    }

    class ALOAD_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ALoadUtil._aload(frame, 0);
        }
    }

    class ALOAD_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ALoadUtil._aload(frame, 1);
        }
    }

    class ALOAD_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ALoadUtil._aload(frame, 2);
        }
    }

    class ALOAD_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ALoadUtil._aload(frame, 3);
        }
    }
}
