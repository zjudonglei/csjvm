﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.loads
{
    class DLoadUtil
    {
        public static void _dload(JFrame frame, uint index)
        {
            double val = frame.localVars.GetDouble(index);
            frame.operandStack.PushDouble(val);
        }
    }

    class DLOAD : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            DLoadUtil._dload(frame, index);
        }
    }

    class DLOAD_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DLoadUtil._dload(frame, 0);
        }
    }

    class DLOAD_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DLoadUtil._dload(frame, 1);
        }
    }

    class DLOAD_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DLoadUtil._dload(frame, 2);
        }
    }

    class DLOAD_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DLoadUtil._dload(frame, 3);
        }
    }
}
