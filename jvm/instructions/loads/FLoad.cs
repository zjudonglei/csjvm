﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.loads
{
    class FLoadUtil
    {
        public static void _fload(JFrame frame, uint index)
        {
            float val = frame.localVars.GetFloat(index);
            frame.operandStack.PushFloat(val);
        }
    }

    class FLOAD : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            FLoadUtil._fload(frame, index);
        }
    }

    class FLOAD_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FLoadUtil._fload(frame, 0);
        }
    }

    class FLOAD_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FLoadUtil._fload(frame, 1);
        }
    }

    class FLOAD_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FLoadUtil._fload(frame, 2);
        }
    }

    class FLOAD_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FLoadUtil._fload(frame, 3);
        }
    }
}
