﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.loads
{
    class ILoadUtil
    {
        public static void _iload(JFrame frame, uint index)
        {
            int val = frame.localVars.GetInt(index);
            frame.operandStack.PushInt(val);
        }
    }

    class ILOAD : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            ILoadUtil._iload(frame, index);
        }
    }

    class ILOAD_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ILoadUtil._iload(frame, 0);
        }
    }

    class ILOAD_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ILoadUtil._iload(frame, 1);
        }
    }

    class ILOAD_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ILoadUtil._iload(frame, 2);
        }
    }

    class ILOAD_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            ILoadUtil._iload(frame, 3);
        }
    }
}
