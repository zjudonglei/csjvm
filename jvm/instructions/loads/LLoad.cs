﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.loads
{
    class LLoadUtil
    {
        public static void _lload(JFrame frame, uint index)
        {
            long val = frame.localVars.GetLong(index);
            frame.operandStack.PushLong(val);
        }
    }

    class LLOAD : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            LLoadUtil._lload(frame, index);
        }
    }

    class LLOAD_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LLoadUtil._lload(frame, 0);
        }
    }

    class LLOAD_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LLoadUtil._lload(frame, 1);
        }
    }

    class LLOAD_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LLoadUtil._lload(frame, 2);
        }
    }

    class LLOAD_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LLoadUtil._lload(frame, 3);
        }
    }
}
