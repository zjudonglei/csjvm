﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.math
{
    class IAND : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            int v1 = stack.PopInt();
            int result = v1 & v2;
            stack.PushInt(result);
        }
    }

    class LAND : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long v2 = stack.PopLong();
            long v1 = stack.PopLong();
            long result = v1 & v2;
            stack.PushLong(result);
        }
    }
}
