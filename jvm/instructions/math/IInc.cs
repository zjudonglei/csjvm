﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.math
{
    class IINC : Instruction
    {
        public uint index;
        public int val;

        public override void FetchOperands(ByteCodeReader reader)
        {
            index = reader.ReadUint8();
            val = reader.ReadInt8();
        }
        public override void Execute(JFrame frame)
        {
            JLocalVars localVars = frame.localVars;
            int val = localVars.GetInt(index);
            val += this.val;
            localVars.SetInt(index, val);
        }
    }
}
