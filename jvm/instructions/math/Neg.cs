﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.math
{
    class DNEG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            double val = stack.PopDouble();
            stack.PushDouble(-val);
        }
    }

    class FNEG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            float val = stack.PopFloat();
            stack.PushFloat(-val);
        }
    }

    class INEG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int val = stack.PopInt();
            stack.PushInt(-val);
        }
    }

    class LNEG : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            long val = stack.PopLong();
            stack.PushLong(-val);
        }
    }
}
