﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.math
{
    class ISHL : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            int v1 = stack.PopInt();
            int s = v2 & 0x1f;
            int result = v1 << s;
            stack.PushInt(result);
        }
    }

    class ISHR : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            int v1 = stack.PopInt();
            int s = v2 & 0x1f;
            int result = v1 >> s;
            stack.PushInt(result);
        }
    }

    class IUSHR : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            uint v1 = (uint)stack.PopInt();
            int s = v2 & 0x1f;
            uint result = v1 >> s;
            stack.PushInt((int)result);
        }
    }

    class LSHL : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            long v1 = stack.PopLong();
            int s = v2 & 0x3F;
            long result = v1 << s;
            stack.PushLong(result);
        }
    }

    class LSHR : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            long v1 = stack.PopLong();
            int s = v2 & 0x1f;
            long result = v1 >> s;
            stack.PushLong(result);
        }
    }

    class LUSHR : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            int v2 = stack.PopInt();
            ulong v1 = (uint)stack.PopLong();
            int s = v2 & 0x1f;
            ulong result = v1 >> s;
            stack.PushLong((long)result);
        }
    }
}
