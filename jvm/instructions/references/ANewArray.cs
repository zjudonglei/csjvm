﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class ANEW_ARRAY : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JConstantPool pool = frame.method.jClass.constantPool;
            ClassRef classRef = pool.GetConstant<ClassRef>(index);
            JClass pointerClass = classRef.ResolvedClass();
            int count = frame.operandStack.PopInt();
            if (count < 0)
            {
                Console.WriteLine("java.lang.NegativeArraySizeException");
                Environment.Exit(0);
            }

            JClass jClass = pointerClass.ArrayClass();
            JObject obj = jClass.NewArray((uint)count);
            frame.operandStack.PushRef(obj);
        }
    }
}
