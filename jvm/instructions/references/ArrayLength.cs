﻿using jvm.instructions.common;
using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class ARRAY_LENGTH : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JObject obj = frame.operandStack.PopRef();
            if (obj == null)
            {
                Console.WriteLine("java.lang.NullPointerException");
                Environment.Exit(0);
            }
            int length = obj.ArrayLength();
            frame.operandStack.PushInt(length);
        }
    }
}
