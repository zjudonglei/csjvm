﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class INSTANCE_OF : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            JObject obj = stack.PopRef();
            if (obj == null)
            {
                stack.PushInt(0);
                return;
            }

            JConstantPool pool = frame.method.jClass.constantPool;
            ClassRef classRef = pool.GetConstant<ClassRef>(index);
            JClass jClass = classRef.ResolvedClass();
            if (obj.IsInstanceOf(jClass))
            {
                stack.PushInt(1);
            }
            else
            {
                stack.PushInt(0);
            }
        }
    }
}
