﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class INVOKE_STATIC : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JClass curClass = frame.method.jClass;
            JConstantPool pool = curClass.constantPool;
            MethodRef methodRef = pool.GetConstant<MethodRef>(index);
            JMethod method = methodRef.ResolvedMethod();

            if (!method.Is(AccessFlags.ACC_STATIC))
            {
                Console.WriteLine("java.lang.IncompatibleClassChangeError");
                Environment.Exit(0);
            }

            JClass jClass = method.jClass;
            if (!jClass.inited)
            {
                frame.RevertPc(); // 待类初始化完毕后继续执行这个指令
                jClass.Init(frame.thread);
                return;
            }
            // 待初始化的几个帧执行完后继续执行当前帧的这个方法
            InvokeUtil.InvokeMethod(frame, method);
        }
    }
}
