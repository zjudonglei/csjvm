﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using jvm.rtda.heap.clazz.method;
using jvm.rtda.heap.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class INVOKE_VIRTUAL : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JClass curClass = frame.method.jClass;
            JConstantPool pool = curClass.constantPool;
            MethodRef methodRef = pool.GetConstant<MethodRef>(index);
            JMethod method = methodRef.ResolvedMethod();

            if (method.Is(AccessFlags.ACC_STATIC))
            {
                Console.WriteLine("java.lang.InCompatibleClassChangeError");
                Environment.Exit(0);
            }

            JObject thisObj = frame.operandStack.GetRefFromTop(method.argCount - 1);
            if (thisObj == null)
            {
                if (methodRef.name.Equals("println"))
                {
                    _println(frame.operandStack, methodRef.descriptor);
                    return;
                }
                Console.WriteLine("java.lang.NullPointerException");
                Environment.Exit(0);
            }

            if (method.Is(AccessFlags.ACC_PROTECTED) &&
                method.jClass.IsSuperClassOf(curClass) &&
                method.jClass.PackageName() != curClass.PackageName() &&
                thisObj.jClass != curClass &&
                !thisObj.jClass.IsSubClassOf(curClass))
            {

                Console.WriteLine("java.lang.IllegalAccessError");
                Environment.Exit(0);
            }

            // 动态绑定
            JMethod methodToBeInvoked = MethodLookUp.LookUpMethodInClass(thisObj.jClass, methodRef.name, methodRef.descriptor);

            if (methodToBeInvoked == null || methodToBeInvoked.Is(AccessFlags.ACC_ABSTRACT))
            {
                Console.WriteLine("java.lang.AbstractMethodError");
                Environment.Exit(0);
            }
            InvokeUtil.InvokeMethod(frame, methodToBeInvoked);
        }

        void _println(JOperandStack stack, string descriptor)
        {
            switch (descriptor)
            {
                case "(Z)V":
                    Console.WriteLine("{0}", stack.PopInt() != 0);
                    break;
                case "(C)V":
                    Console.WriteLine("{0}", stack.PopInt());
                    break;
                case "(I)V":
                case "(B)V":
                case "(S)V":
                    Console.WriteLine("{0}", stack.PopInt());
                    break;
                case "(F)V":
                    Console.WriteLine("{0}", stack.PopFloat());
                    break;
                case "(J)V":
                    Console.WriteLine("{0}", stack.PopLong());
                    break;
                case "(D)V":
                    Console.WriteLine("{0}", stack.PopDouble());
                    break;
                case "(Ljava/lang/String;)V":
                    JObject strObj = stack.PopRef();
                    Console.WriteLine(StringPool.String(strObj));
                    break;
                default:
                    Console.WriteLine("{0}", descriptor);
                    break;
            }
            stack.PopRef();
        }
    }
}
