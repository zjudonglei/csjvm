﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class PUT_STATIC : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JMethod method = frame.method;
            JClass curClass = method.jClass;
            JConstantPool pool = curClass.constantPool;
            FieldRef fieldRef = pool.GetConstant<FieldRef>(index);
            JField field = fieldRef.ResolvedField();
            JClass jClass = field.jClass;
            if (!jClass.inited)
            {
                frame.RevertPc();
                jClass.Init(frame.thread);
                return;
            }
            // todo init class
            if (!field.Is(AccessFlags.ACC_STATIC))
            {
                Console.WriteLine("java.lang.IncompatibleClassChangeError");
                Environment.Exit(0);
            }
            if (field.Is(AccessFlags.ACC_FINAL))
            {
                if (curClass != jClass || !method.name.Equals("<clinit>"))
                {
                    Console.WriteLine("java.lang.IllegalAccessError");
                    Environment.Exit(0);
                }
            }

            char descriptor = field.descriptor[0];
            uint slotId = field.slotId;
            Slots slots = jClass.staticVars;
            JOperandStack stack = frame.operandStack;
            switch (descriptor)
            {
                case 'Z':
                case 'B':
                case 'C':
                case 'S':
                case 'I':
                    slots.SetInt(slotId, stack.PopInt());
                    break;
                case 'F':
                    slots.SetFloat(slotId, stack.PopFloat());
                    break;
                case 'J':
                    slots.SetLong(slotId, stack.PopLong());
                    break;
                case 'D':
                    slots.SetDouble(slotId, stack.PopDouble());
                    break;
                case 'L':
                case '[':
                    slots.SetRef(slotId, stack.PopRef());
                    break;
            }
        }
    }
}
