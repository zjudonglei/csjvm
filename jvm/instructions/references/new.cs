﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.cp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.references
{
    class NEW : Index16Instruction
    {
        public override void Execute(JFrame frame)
        {
            JConstantPool pool = frame.method.jClass.constantPool;
            ClassRef classRef = pool.GetConstant<ClassRef>(index);
            JClass clazz = classRef.ResolvedClass();
            if (!clazz.inited)
            {
                frame.RevertPc();
                clazz.Init(frame.thread);
                return;
            }
            // todo: init class
            if (clazz.Is(AccessFlags.ACC_INTERFACE) || clazz.Is(AccessFlags.ACC_ABSTRACT))
            {
                Console.WriteLine("java.lang.InstantiationError");
                Environment.Exit(0);
            }
            JObject obj = clazz.NewObject();
            frame.operandStack.PushRef(obj);
        }
    }
}
