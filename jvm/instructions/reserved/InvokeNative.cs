﻿using jvm.instructions.common;
using jvm.native;
using jvm.rtda;
using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.reserved
{
    class INVOKE_NATIVE : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JMethod method = frame.method;
            string classname = method.jClass.name;
            string methodname = method.name;
            string descriptor = method.descriptor;
            NativeMethod nativeMethod = NativeMethodPool.FindNativeMethod(classname, methodname, descriptor);
            if (nativeMethod == null)
            {
                string key = classname + "." + methodname + descriptor;
                Console.WriteLine("java.lang.UnsatisfiedLinkError: " + key);
                Environment.Exit(0);
                return;
            }
            nativeMethod(frame);
        }
    }
}
