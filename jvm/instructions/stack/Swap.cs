﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stack
{
    class SWAP : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            JOperandStack stack = frame.operandStack;
            Slot slot1 = stack.PopSlot();
            Slot slot2 = stack.PopSlot();
            stack.PushSlot(slot1);
            stack.PushSlot(slot2);
        }
    }
}
