﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stores
{
    class AStoreUtil
    {
        public static void _astore(JFrame frame, uint index)
        {
            JObject obj = frame.operandStack.PopRef();
            frame.localVars.SetRef(index, obj);
        }
    }

    class ASTORE : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            AStoreUtil._astore(frame, index);
        }
    }

    class ASTORE_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            AStoreUtil._astore(frame, 0);
        }
    }

    class ASTORE_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            AStoreUtil._astore(frame, 1);
        }
    }

    class ASTORE_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            AStoreUtil._astore(frame, 2);
        }
    }

    class ASTORE_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            AStoreUtil._astore(frame, 3);
        }
    }
}
