﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stores
{
    class DStoreUtil
    {
        public static void _dstore(JFrame frame, uint index)
        {
            double val = frame.operandStack.PopDouble();
            frame.localVars.SetDouble(index, val);
        }
    }

    class DSTORE : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            DStoreUtil._dstore(frame, index);
        }
    }

    class DSTORE_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DStoreUtil._dstore(frame, 0);
        }
    }

    class DSTORE_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DStoreUtil._dstore(frame, 1);
        }
    }

    class DSTORE_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DStoreUtil._dstore(frame, 2);
        }
    }

    class DSTORE_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            DStoreUtil._dstore(frame, 3);
        }
    }
}
