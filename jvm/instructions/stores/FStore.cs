﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stores
{
    class FStoreUtil
    {
        public static void _fstore(JFrame frame, uint index)
        {
            float val = frame.operandStack.PopFloat();
            frame.localVars.SetFloat(index, val);
        }
    }

    class FSTORE : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            FStoreUtil._fstore(frame, index);
        }
    }

    class FSTORE_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FStoreUtil._fstore(frame, 0);
        }
    }

    class FSTORE_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FStoreUtil._fstore(frame, 1);
        }
    }

    class FSTORE_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FStoreUtil._fstore(frame, 2);
        }
    }

    class FSTORE_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            FStoreUtil._fstore(frame, 3);
        }
    }
}
