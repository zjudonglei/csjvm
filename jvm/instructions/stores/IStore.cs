﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stores
{
    class IStoreUtil
    {
        public static void _istore(JFrame frame, uint index)
        {
            int val = frame.operandStack.PopInt();
            frame.localVars.SetInt(index, val);
        }
    }

    class ISTORE : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            IStoreUtil._istore(frame, index);
        }
    }

    class ISTORE_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            IStoreUtil._istore(frame, 0);
        }
    }

    class ISTORE_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            IStoreUtil._istore(frame, 1);
        }
    }

    class ISTORE_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            IStoreUtil._istore(frame, 2);
        }
    }

    class ISTORE_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            IStoreUtil._istore(frame, 3);
        }
    }
}
