﻿using jvm.instructions.common;
using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.instructions.stores
{
    class LStoreUtil
    {
        public static void _lstore(JFrame frame, uint index)
        {
            long val = frame.operandStack.PopLong();
            frame.localVars.SetLong(index, val);
        }
    }

    class LSTORE : Index8Instruction
    {
        public override void Execute(JFrame frame)
        {
            LStoreUtil._lstore(frame, index);
        }
    }

    class LSTORE_0 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LStoreUtil._lstore(frame, 0);
        }
    }

    class LSTORE_1 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LStoreUtil._lstore(frame, 1);
        }
    }

    class LSTORE_2 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LStoreUtil._lstore(frame, 2);
        }
    }

    class LSTORE_3 : NoOperandsInstruction
    {
        public override void Execute(JFrame frame)
        {
            LStoreUtil._lstore(frame, 3);
        }
    }
}
