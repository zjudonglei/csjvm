﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace jvm.mm
{
    class GcSpace
    {
        int max_size;
        IntPtr startPtr;
        IntPtr curPtr;
        public int curSize;

        public GcSpace(int size)
        {
            max_size = size;
            startPtr = Marshal.AllocHGlobal(max_size);
            curPtr = startPtr;
            curSize = 0;
        }

        public JObject Alloc(JObject obj)
        {
            int size = Marshal.SizeOf(obj);
            Marshal.StructureToPtr(obj, curPtr, true);
            obj = (JObject)Marshal.PtrToStructure(curPtr, typeof(JObject));
            obj.intPtr = curPtr;
            curPtr = curPtr + size;
            curSize += size;
            return obj;
        }

        public bool IsFull(int size)
        {
            if ((curPtr + size).ToInt32() >= (startPtr + max_size).ToInt32())
            {
                return true;
            }
            return curSize + size >= max_size;
        }

        public void Clear()
        {
            curPtr = startPtr;
            curSize = 0;
        }
    }
}
