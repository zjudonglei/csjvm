﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native
{
    delegate void NativeMethod(JFrame frame);
    class NativeMethodPool
    {
        static Dictionary<string, NativeMethod> pool = new Dictionary<string, NativeMethod>();

        public static void Init()
        {
            java.lang.Class.Init();
            java.lang.Double.Init();
            java.lang.Float.Init();
            java.lang.Object.Init();
            java.lang.String.Init();
            java.lang.System.Init();
            java.lang.Throwable.Init();
            sun.misc.VM.Init();
        }

        public static void Register(string classname, string methodname, string descriptor, NativeMethod nativeMethod)
        {
            string key = classname + "~" + methodname + "~" + descriptor;
            pool[key] = nativeMethod;
        }

        static void EmptyMethod(JFrame frame)
        {

        }

        public static NativeMethod FindNativeMethod(string classname, string methodname, string descriptor)
        {
            string key = classname + "~" + methodname + "~" + descriptor;
            if (pool.ContainsKey(key))
            {
                return pool[key];
            }
            // private static native void registerNatives();
            if (methodname.Equals("registerNatives") && descriptor.Equals("()V"))
            {
                return EmptyMethod;
            }
            return null;
        }
    }
}
