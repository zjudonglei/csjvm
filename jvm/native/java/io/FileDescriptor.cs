﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.io
{
    class FileDescriptor
    {
        static string classname = "java/io/FileDescriptor";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "set", "(I)J", set);
        }

        // private static native long set(int d);
        // (I)J
        static void set(JFrame frame)
        {
            // todo
        }
    }
}
