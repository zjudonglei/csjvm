﻿using jvm.rtda;
using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.io
{
    class FileOutputStream
    {
        static string classname = "java/io/FileOutputStream";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "writeBytes", "([BIIZ)V", writeBytes);
        }

        // private native void writeBytes(byte b[], int off, int len, boolean append) throws IOException;
        // ([BIIZ)V
        static void writeBytes(JFrame frame)
        {
            JLocalVars localVars = frame.localVars;
            //JObject thisObject = localVars.GetRef(0);
            JObject bObj = localVars.GetRef(1);
            int off = localVars.GetInt(2);
            int len = localVars.GetInt(3);
            //bool append = localVars.GetBoolean(4);
            byte[] bytes = bObj.Bytes();
            char[] chars = new char[len];
            for (int i = off, j = 0; i < off + len; i++, j++)
            {
                chars[j] = (char)bytes[i];
            }
            Console.WriteLine(chars);
        }
    }
}
