﻿using jvm.rtda;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class Class
    {
        static string classname = "java/lang/Class";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "getPrimitiveClass", "(Ljava/lang/String;)Ljava/lang/Class;", getPrimitiveClass);
            NativeMethodPool.Register(classname, "getName0", "()Ljava/lang/String;", getName0);
            NativeMethodPool.Register(classname, "desiredAssertionStatus0", "(Ljava/lang/Class;)Z", desiredAssertionStatus0);
        }

        // static native Class<?> getPrimitiveClass(String name);
        // (Ljava/lang/String;)Ljava/lang/Class;
        static void getPrimitiveClass(JFrame frame)
        {
            JObject strObject = frame.localVars.GetRef(0);
            string name = StringPool.String(strObject);
            JClassLoader loader = frame.method.jClass.loader;
            JClass jClass = loader.LoadClass(name);
            JObject jLClassObject = jClass.jLClassObject;
            frame.operandStack.PushRef(jLClassObject);
        }

        // private native String getName0();
        // ()Ljava/lang/String;
        static void getName0(JFrame frame)
        {
            JObject thisObject = frame.localVars.GetThis();
            JClass extClass = thisObject.extClass;
            string name = extClass.JavaName;
            JObject strObject = StringPool.StringObject(extClass.loader, name);
            frame.operandStack.PushRef(strObject);
        }

        // private static native boolean desiredAssertionStatus0(Class<?> clazz);
        // (Ljava/lang/Class;)Z
        static void desiredAssertionStatus0(JFrame frame)
        {
            frame.operandStack.PushBoolean(false);
        }
    }
}
