﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class Double
    {
        static string classname = "java/lang/Double";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "doubleToRawLongBits", "(D)J", doubleToRawLongBits);
            NativeMethodPool.Register(classname, "longBitsToDouble", "(J)D", longBitsToDouble);
        }

        // public static native long doubleToRawLongBits(double value);
        // (D)J
        static void doubleToRawLongBits(JFrame frame)
        {
            double d = frame.localVars.GetDouble(0);
            long j = BitConverter.ToInt64(BitConverter.GetBytes(d), 0);
            frame.operandStack.PushLong(j);
        }

        // public static native double longBitsToDouble(long bits);
        // (J)D
        static void longBitsToDouble(JFrame frame)
        {
            long j = frame.localVars.GetInt(0);
            double f = BitConverter.ToDouble(BitConverter.GetBytes(j), 0);
            frame.operandStack.PushDouble(f);
        }
    }
}
