﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class Float
    {
        static string classname = "java/lang/Float";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "floatToRawIntBits", "(F)I", floatToRawIntBits);
            NativeMethodPool.Register(classname, "intBitsToFloat", "(I)F", intBitsToFloat);
        }

        // public static native int floatToRawIntBits(float value);
        // (F)I
        static void floatToRawIntBits(JFrame frame)
        {
            float f = frame.localVars.GetFloat(0);
            int i = BitConverter.ToInt32(BitConverter.GetBytes(f), 0);
            frame.operandStack.PushInt(i);
        }

        // public static native float intBitsToFloat(int bits);
        // (I)F
        static void intBitsToFloat(JFrame frame)
        {
            int i = frame.localVars.GetInt(0);
            float f = BitConverter.ToSingle(BitConverter.GetBytes(i), 0);
            frame.operandStack.PushFloat(f);
        }
    }
}
