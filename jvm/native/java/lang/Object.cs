﻿using jvm.rtda;
using jvm.rtda.heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class Object
    {
        static string classname = "java/lang/Object";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "getClass", "()Ljava/lang/Class;", getClass);
            NativeMethodPool.Register(classname, "hashCode", "()I", hashCode);
            NativeMethodPool.Register(classname, "clone", "()Ljava/lang/Object;", clone);
        }

        // public final native Class<?> getClass();
        // ()Ljava/lang/Class;
        static void getClass(JFrame frame)
        {
            JObject thisObject = frame.localVars.GetThis();
            JObject jLClassObject = thisObject.jClass.jLClassObject;
            frame.operandStack.PushRef(jLClassObject);
        }

        // public native int hashCode();
        // ()I
        static unsafe void hashCode(JFrame frame)
        {
            JObject thisObject = frame.localVars.GetThis();
            frame.operandStack.PushInt(thisObject.intPtr.ToInt32());
        }

        // protected native Object clone() throws CloneNotSupportedException;
        // ()Ljava/lang/Object;
        static void clone(JFrame frame)
        {
            JObject thisObject = frame.localVars.GetThis();
            JClass jClass = thisObject.jClass.loader.LoadClass("java/lang/Cloneable");
            if (!thisObject.jClass.IsImplements(jClass))
            {
                Console.WriteLine("java.lang.CloneNotSupportedException");
                Environment.Exit(0);
            }
            frame.operandStack.PushRef(thisObject.Clone());
        }

    }
}
