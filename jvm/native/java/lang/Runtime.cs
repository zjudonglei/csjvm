﻿using jvm.rtda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class Runtime
    {
        static string classname = "java/lang/Class";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "getPrimitiveClass", "(Ljava/lang/String;)Ljava/lang/Class;", getPrimitiveClass);
        }

        // static native Class<?> getPrimitiveClass(String name);
        // (Ljava/lang/String;)Ljava/lang/Class;
        static void getPrimitiveClass(JFrame frame)
        {

        }
    }
}
