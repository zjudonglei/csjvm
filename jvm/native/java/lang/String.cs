﻿using jvm.rtda;
using jvm.rtda.heap.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.java.lang
{
    class String
    {
        static string classname = "java/lang/String";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "intern", "()Ljava/lang/String;", intern);
        }

        // public native String intern();
        // ()Ljava/lang/String;
        static void intern(JFrame frame)
        {
            JObject thisObject = frame.localVars.GetThis();
            JObject strObj = StringPool.StringObject(thisObject);
            frame.operandStack.PushRef(strObj);
        }
    }
}
