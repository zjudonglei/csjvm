﻿using jvm.instructions.references;
using jvm.rtda;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.native.sun.misc
{
    class VM
    {
        static string classname = "sun/misc/VM";
        public static void Init()
        {
            NativeMethodPool.Register(classname, "initialize", "()V", initialize);
        }

        // private static native void initialize();
        // ()V
        static void initialize(JFrame frame)
        {
            JClassLoader loader = frame.method.jClass.loader;
            JClass systemClass = loader.LoadClass("java/lang/System");
            JMethod initMethod = systemClass.FindInstanceMethod("initializeSystemClass", "()V");
            InvokeUtil.InvokeMethod(frame, initMethod);
        }
    }
}
