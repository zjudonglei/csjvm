﻿using jvm.rtda.frame;
using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda
{
    class JFrame
    {
        public JFrame next;
        public JLocalVars localVars;
        public JOperandStack operandStack;
        public JThread thread;
        public JMethod method;
        public int nextPc;

        public JFrame(JThread thread, JMethod method)
        {
            this.thread = thread;
            this.method = method;
            localVars = new JLocalVars(method.maxLocals);
            operandStack = new JOperandStack(method.maxStack);
        }

        public void Branch(int offset)
        {
            int pc = thread.pc;
            nextPc = (pc + offset);
        }

        public void RevertPc()
        {
            nextPc = thread.pc;
        }
    }
}
