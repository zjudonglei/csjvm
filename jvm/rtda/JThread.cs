﻿using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace jvm.rtda
{
    class JThread
    {
        public int pc;
        public JvmStack stack;
        public Thread sysThread;

        public JThread()
        {
            stack = new JvmStack(1024);
        }

        public JFrame NewFrame(JMethod method)
        {
            return new JFrame(this, method);
        }

        public void PushFrame(JFrame frame)
        {
            stack.Push(frame);
        }

        public JFrame PopFrame()
        {
            return stack.Pop();
        }

        public JFrame TopFrame()
        {
            return stack.Top();
        }

        public JFrame CurrentFrame()
        {
            return TopFrame();
        }

        public bool IsEmpty()
        {
            return stack._top == null;
        }

        public void ClearStack()
        {
            stack.Clear();
        }

        public JFrame[] GetFrames()
        {
            return stack.GetFrames(0);
        }

        public JFrame[] GetFrames(int skip)
        {
            return stack.GetFrames(skip);
        }
    }
}
