﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.frame
{
    class JLocalVars : Slots
    {
        public JLocalVars(uint size) : base(size)
        {
        }

        public JObject GetThis()
        {
            return GetRef(0);
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[JLocalVars:");
            builder.Append(base.ToString());
            builder.Append("]");
            return builder.ToString();
        }
    }
}
