﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.frame
{
    class Slots
    {
        public Slot[] slots;

        public Slots(uint size)
        {
            slots = new Slot[size];
            for (int i = 0; i < size; i++)
            {
                slots[i] = new Slot();
            }
        }

        public void SetInt(uint index, int value)
        {
            slots[index].value = value;
        }

        public void SetFloat(uint index, float value)
        {
            slots[index].value = BitConverter.ToInt32(BitConverter.GetBytes(value), 0);
        }

        public void SetLong(uint index, long value)
        {
            slots[index].value = BitConverter.ToInt32(BitConverter.GetBytes(value), 0);
            slots[++index].value = BitConverter.ToInt32(BitConverter.GetBytes(value), 4);
        }

        public void SetDouble(uint index, double value)
        {
            slots[index].value = BitConverter.ToInt32(BitConverter.GetBytes(value), 0);
            slots[++index].value = BitConverter.ToInt32(BitConverter.GetBytes(value), 4);
        }

        public void SetRef(uint index, JObject obj)
        {
            slots[index].reference = obj;
        }

        public void SetSlot(uint index, Slot slot)
        {
            slots[index] = slot;
        }

        public bool GetBoolean(uint index)
        {
            return slots[index].value == 1;
        }

        public int GetInt(uint index)
        {
            return slots[index].value;
        }

        public float GetFloat(uint index)
        {
            return BitConverter.ToSingle(BitConverter.GetBytes(slots[index].value), 0);
        }

        public long GetLong(uint index)
        {
            byte[] bytes = BitConverter.GetBytes(slots[index].value).Concat(BitConverter.GetBytes(slots[++index].value)).ToArray();
            return BitConverter.ToInt64(bytes, 0);
        }

        public double GetDouble(uint index)
        {
            byte[] bytes = BitConverter.GetBytes(slots[index].value).Concat(BitConverter.GetBytes(slots[++index].value)).ToArray();
            return BitConverter.ToDouble(bytes, 0);
        }

        public JObject GetRef(uint index)
        {
            return slots[index]?.reference;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i] == null)
                {
                    continue;
                }
                builder.Append(" (");
                if (slots[i].reference != null)
                {
                    builder.Append(slots[i].reference);
                }
                else
                {
                    builder.Append(slots[i].value);
                }
                builder.Append(") ");
            }
            return builder.ToString();
        }
    }
}
