﻿using jvm.rtda.frame;
using jvm.rtda.heap;
using jvm.rtda.heap.clazz;
using jvm.rtda.heap.clazz.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda
{
    /// <summary>
    /// object共用class的方法区等信息
    /// 不共用字段值
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    partial class JObject
    {
        public JClass jClass; // 模板
        public object data; // 变量
        public JClass extClass; // 当JClass为java/lang/Class时此字段生效，标明代表了那个类的实体
        public IntPtr intPtr;
        public object exceptionInfo; // 额外信息，包括行号等
        public long gcNumber;
        public ObjectReference weakreference = new ObjectReference();
        public bool inOld;

        public JObject()
        {
        }


        public JObject(JClass jClass)
        {
            this.jClass = jClass;
            data = new Slots(jClass.instanceSlotCount);
        }

        public JObject(JClass jClass, object obj)
        {
            this.jClass = jClass;
            data = obj;
        }

        public Slots Fields
        {
            get { return (Slots)data; }
        }

        public bool IsInstanceOf(JClass jClass)
        {
            return jClass.IsParentOf(this.jClass);
        }

        // 反射
        public JObject GetRefVar(string name, string descriptor)
        {
            JField field = jClass.FindField(name, descriptor);
            Slots slots = (Slots)data;
            return slots.GetRef(field.slotId);
        }

        public void SetRefVar(string name, string descriptor, JObject obj)
        {
            JField field = jClass.FindField(name, descriptor);
            Slots slots = (Slots)data;
            slots.SetRef(field.slotId, obj);
        }
    }
}
