﻿using jvm.classfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz
{
    class ClassMember
    {
        public ushort accessFlags;
        public string name;
        public string descriptor;

        public JClass jClass;

        public ClassMember(JClass jClass, JMemberInfo memberInfo)
        {
            this.jClass = jClass;
            accessFlags = memberInfo.access_flags;
            name = memberInfo.Name();
            descriptor = memberInfo.Descriptor();
        }

        public bool Is(ushort flag)
        {
            return (accessFlags & flag) != 0;
        }

        public bool IsAccessibleTo(JClass d)
        {
            if (Is(AccessFlags.ACC_PUBLIC))
            {
                return true;
            }
            if (Is(AccessFlags.ACC_PROTECTED))
            {
                return jClass == d || d.IsSubClassOf(jClass) || jClass.PackageName().Equals(d.PackageName());
            }
            if (!Is(AccessFlags.ACC_PRIVATE))
            {
                return jClass.PackageName().Equals(d.PackageName());
            }
            return jClass == d;
        }
    }
}
