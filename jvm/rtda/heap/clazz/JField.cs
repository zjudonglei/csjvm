﻿using jvm.classfile;
using jvm.classfile.attributeinfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz
{
    class JField : ClassMember
    {
        public uint constValueIndex; // 初始化的常量
        public uint slotId;

        public JField(JClass jClass, JMemberInfo memberInfo) : base(jClass, memberInfo)
        {
            ConstantValueAttribute attr = memberInfo.ConstantValueAttribute();
            if (attr != null)
            {
                constValueIndex = attr.constantValueIndex;
            }
        }

        public bool IsLongOrDouble()
        {
            return descriptor.Equals("J") || descriptor.Equals("D");
        }
    }
}
