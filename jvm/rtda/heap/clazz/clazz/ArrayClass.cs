﻿using jvm.mm;
using jvm.rtda.heap.clazz.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap
{
    partial class JClass
    {
        public JObject NewArray(uint count)
        {
            if (!IsArray())
            {
                Console.WriteLine("Not array class {0}", name);
                Environment.Exit(0);
            }
            switch (name)
            {
                case "[Z":
                    return MemoryManager.Alloc(new JObject(this, new byte[count]));
                case "[B":
                    return MemoryManager.Alloc(new JObject(this, new byte[count]));
                case "[C":
                    return MemoryManager.Alloc(new JObject(this, new char[count]));
                case "[S":
                    return MemoryManager.Alloc(new JObject(this, new short[count]));
                case "[I":
                    return MemoryManager.Alloc(new JObject(this, new int[count]));
                case "[J":
                    return MemoryManager.Alloc(new JObject(this, new long[count]));
                case "[F":
                    return MemoryManager.Alloc(new JObject(this, new float[count]));
                case "[D":
                    return MemoryManager.Alloc(new JObject(this, new double[count]));
                default:
                    return MemoryManager.Alloc(new JObject(this, new JObject[count]));
            }
        }

        public JClass GetInnerClass()
        {
            string innerName = ClassNameHelper.GetInnerClassName(name);
            return loader.LoadClass(innerName);
        }

        public bool IsArray()
        {
            return name[0] == '[';
        }

        public bool IsPrimitive()
        {
            return ClassNameHelper.map.ContainsKey(name);
        }

    }
}
