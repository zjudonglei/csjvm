﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda
{
    partial class JObject
    {
        public byte[] Bytes()
        {
            return (byte[])data;
        }

        public short[] Shorts()
        {
            return (short[])data;
        }

        public char[] Chars()
        {
            return (char[])data;
        }

        public int[] Ints()
        {
            return (int[])data;
        }

        public long[] Longs()
        {
            return (long[])data;
        }

        public float[] Floats()
        {
            return (float[])data;
        }

        public double[] Doubles()
        {
            return (double[])data;
        }

        public JObject[] Refs()
        {
            return (JObject[])data;
        }

        public int ArrayLength()
        {
            if (data.GetType() == typeof(byte[]))
            {
                return Bytes().Length;
            }
            else if (data.GetType() == typeof(short[]))
            {
                return Shorts().Length;
            }
            else if (data.GetType() == typeof(char[]))
            {
                return Chars().Length;
            }
            else if (data.GetType() == typeof(int[]))
            {
                return Ints().Length;
            }
            else if (data.GetType() == typeof(long[]))
            {
                return Longs().Length;
            }
            else if (data.GetType() == typeof(float[]))
            {
                return Floats().Length;
            }
            else if (data.GetType() == typeof(double[]))
            {
                return Doubles().Length;
            }
            else if (data.GetType() == typeof(JObject[]))
            {
                return Refs().Length;
            }
            else
            {
                Console.WriteLine("Not Array");
                Environment.Exit(0);
                return 0;
            }
        }

        public static void ArrayCopy(JObject src, JObject dest, int srcPos, int destPos, int length)
        {
            if (src.data.GetType() == typeof(byte[]))
            {
                byte[] _src = (byte[])src.data;
                byte[] _dest = (byte[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(short[]))
            {
                short[] _src = (short[])src.data;
                short[] _dest = (short[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(char[]))
            {
                char[] _src = (char[])src.data;
                char[] _dest = (char[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(int[]))
            {
                int[] _src = (int[])src.data;
                int[] _dest = (int[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(long[]))
            {
                long[] _src = (long[])src.data;
                long[] _dest = (long[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(float[]))
            {
                float[] _src = (float[])src.data;
                float[] _dest = (float[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(double[]))
            {
                double[] _src = (double[])src.data;
                double[] _dest = (double[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else if (src.data.GetType() == typeof(JObject[]))
            {
                JObject[] _src = (JObject[])src.data;
                JObject[] _dest = (JObject[])dest.data;
                for (int i = 0; i < length; i++)
                {
                    _dest[destPos + i] = _src[srcPos + i];
                }
            }
            else
            {
                Console.WriteLine("Not Array!");
                Environment.Exit(0);
            }
        }
    }
}
