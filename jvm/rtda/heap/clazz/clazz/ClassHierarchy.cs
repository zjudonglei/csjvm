﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap
{
    partial class JClass
    {
        public bool IsParentOf(JClass other)
        {
            if (this == other)
            {
                return true;
            }
            if (other.IsArray())
            {
                // 两个数组剥离掉数组后比较
                if (IsArray())
                {
                    JClass otherInnerClass = other.GetInnerClass();
                    JClass innerClass = GetInnerClass();
                    return otherInnerClass == innerClass || innerClass.IsParentOf(otherInnerClass);
                }
                else
                {
                    // 数组是Object，实现了clone和serial接口
                    if (Is(AccessFlags.ACC_INTERFACE))
                    {
                        return IsJLCloneable() || IsJLSerializable();
                    }
                    else
                    {
                        return IsJLObject();
                    }
                }
            }
            else
            {
                // 接口只能继承接口，接口也是Object
                if (other.Is(AccessFlags.ACC_INTERFACE))
                {
                    if (Is(AccessFlags.ACC_INTERFACE))
                    {
                        return other.IsSubClassOf(this);
                    }
                    else
                    {
                        return IsJLObject();
                    }
                }
                else // 子类或者实现本接口
                {
                    if (Is(AccessFlags.ACC_INTERFACE))
                    {
                        return other.IsImplements(this);
                    }
                    else
                    {
                        return other.IsSubClassOf(this);
                    }
                }
            }

        }

        public bool IsSubClassOf(JClass other)
        {
            for (JClass c = superClass; c != null; c = c.superClass)
            {
                if (c == other)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSuperClassOf(JClass other)
        {
            return other.IsSubClassOf(this);
        }

        public bool IsImplements(JClass iface)
        {
            for (JClass c = this; c != null; c = c.superClass)
            {
                foreach (JClass i in interfaces)
                {
                    if (i == iface || i.IsSubInterfaceOf(iface))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSubInterfaceOf(JClass iface)
        {
            foreach (JClass superInterface in interfaces)
            {
                if (superInterface == iface || superInterface.IsSubInterfaceOf(iface))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
