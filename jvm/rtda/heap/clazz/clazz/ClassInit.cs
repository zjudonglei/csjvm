﻿using jvm.rtda.heap.clazz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap
{
    partial class JClass
    {
        public void Init(JThread thread)
        {
            inited = true;
            ScheduleClinit(thread);
            InitSuperClass(thread);
        }

        void ScheduleClinit(JThread thread)
        {
            JMethod clinit = FindClinitMethod();
            if (clinit != null)
            {
                JFrame newFrame = thread.NewFrame(clinit);
                thread.PushFrame(newFrame);
            }
        }

        void InitSuperClass(JThread thread)
        {
            if (!Is(AccessFlags.ACC_INTERFACE))
            {
                JClass baseClass = superClass;
                if (baseClass != null && !baseClass.inited)
                {
                    baseClass.Init(thread);
                }
            }
        }
    }
}
