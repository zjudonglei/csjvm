﻿using jvm.rtda.frame;
using jvm.rtda.heap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda
{
    partial class JObject
    {
        public JObject Clone()
        {
            JObject clone = new JObject();
            clone.jClass = jClass;
            clone.data = CloneData();

            return this;
        }

        object CloneData()
        {
            if (data.GetType() == typeof(byte[]))
            {
                byte[] _src = (byte[])data;
                byte[] _dest = new byte[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(short[]))
            {
                short[] _src = (short[])data;
                short[] _dest = new short[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(char[]))
            {
                char[] _src = (char[])data;
                char[] _dest = new char[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(int[]))
            {
                int[] _src = (int[])data;
                int[] _dest = new int[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(long[]))
            {
                long[] _src = (long[])data;
                long[] _dest = new long[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(float[]))
            {
                float[] _src = (float[])data;
                float[] _dest = new float[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(double[]))
            {
                double[] _src = (double[])data;
                double[] _dest = new double[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else if (data.GetType() == typeof(JObject[]))
            {
                JObject[] _src = (JObject[])data;
                JObject[] _dest = new JObject[_src.Length];
                _src.CopyTo(_dest, 0);
                return _dest;
            }
            else
            {
                Slots _src = (Slots)data;
                Slots _dest = new Slots((uint)_src.slots.Length);
                _src.slots.CopyTo(_dest.slots, 0);
                return _dest;
            }
        }
    }
}
