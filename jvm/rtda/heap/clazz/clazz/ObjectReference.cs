﻿using jvm.rtda.frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.clazz
{
    class ObjectReference
    {
        public Slot[] slots = new Slot[20];
        int index = 0;

        public void Add(Slot slot)
        {
            if (index >= slots.Length)
            {
                Slot[] newArray = new Slot[slots.Length + 20];
                slots.CopyTo(newArray, 0);
                slots = newArray;
            }
            slots[index] = slot;
            index++;
        }

        public void Clear()
        {
            slots = new Slot[20];
            index = 0;
        }
    }
}
