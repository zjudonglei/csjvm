﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.cp
{
    class ClassRef : SymRef
    {
        public ClassRef(JConstantPool cp, string className) : base(cp, className)
        {
        }
    }
}
