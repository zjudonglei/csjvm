﻿using jvm.classfile.constantpool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.cp
{
    class FieldRef : MemberRef
    {
        private JField field; // 指针缓存

        public FieldRef(JConstantPool cp, ConstantMemberrefInfo memberrefInfo) : base(cp, memberrefInfo)
        {
        }

        public JField ResolvedField()
        {
            if (field == null)
            {
                ResolveFieldRef();
            }
            return field;
        }

        void ResolveFieldRef()
        {
            JClass d = pool.jClass;
            JClass c = ResolvedClass();
            JField field = c.FindField(name, descriptor);
            if (field == null)
            {
                Console.WriteLine("java.lang.NoSuchFieldError");
            }
            if (!field.IsAccessibleTo(d))
            {
                Console.WriteLine("java.lang.IllegalAccessError");
            }
            this.field = field;
        }
    }
}
