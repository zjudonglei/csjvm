﻿using jvm.classfile.constantpool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.cp
{
    class MemberRef : SymRef
    {
        public string name;
        public string descriptor;

        public MemberRef(JConstantPool cp, ConstantMemberrefInfo memberrefInfo) : base(cp, memberrefInfo.ClassName())
        {
            Tuple<string, string> tuple = memberrefInfo.pool.GetNameAndType(memberrefInfo.nameAndTypeIndex);
            name = tuple.Item1;
            descriptor = tuple.Item2;
        }
    }
}
