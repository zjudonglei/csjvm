﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.cp
{
    class SymRef
    {
        public JConstantPool pool;
        public string className; // 所有的引用都包含类，不管是字段还是方法都是定义在类里面的
        private JClass jClass;

        public SymRef(JConstantPool pool, string className)
        {
            this.pool = pool;
            this.className = className;
        }

        public JClass ResolvedClass()
        {
            if (jClass == null)
            {
                ResolveClassRef();
            }
            return jClass;
        }

        private void ResolveClassRef()
        {
            JClass d = pool.jClass;
            JClass c = d.loader.LoadClass(className);
            if (!c.IsAccessibleTo(d))
            {
                Console.WriteLine("java.lang.IllegalAccessError");
                Environment.Exit(0);
            }
            jClass = c;
        }
    }
}
