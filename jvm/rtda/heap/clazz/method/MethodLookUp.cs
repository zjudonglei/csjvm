﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jvm.rtda.heap.clazz.method
{
    class MethodLookUp
    {
        public static JMethod LookUpMethodInClass(JClass jClass, string name, string descriptor)
        {
            for (JClass c = jClass; c != null; c = c.superClass)
            {
                if (c.methods == null)
                {
                    continue;
                }
                foreach (JMethod method in c.methods)
                {
                    if (method.name.Equals(name) && method.descriptor.Equals(descriptor))
                    {
                        return method;
                    }
                }
            }
            return null;
        }

        public static JMethod LookUpMethodInInterfaces(JClass[] ifaces, string name, string descriptor)
        {
            foreach (JClass iface in ifaces)
            {
                foreach (JMethod method in iface.methods)
                {
                    if (method.name.Equals(name) && method.descriptor.Equals(descriptor))
                    {
                        return method;
                    }
                }
                JMethod method0 = LookUpMethodInInterfaces(iface.interfaces, name, descriptor);
                if (method0 != null)
                {
                    return method0;
                }
            }
            return null;
        }
    }
}
